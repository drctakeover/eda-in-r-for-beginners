---
title: "Exploratory Data Analysis in R 102"
output:
  pdf_document: default
  html_notebook: default
---

######################################################################################################################################################################################
######################################################################################################################################################################################
#############################################################################        TRICKY HISTOGRAMS       #########################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################

```{r}
# Histograms can be misleading
hist(train$Age,
     breaks=10,
     xlab="Age",
     main="Histogram w/ 10 breaks"
     )

hist(train$Age,
     breaks=20,
     xlab="Age",
     main="Histogram w/ 20 breaks"
     )

hist(train$Age,
     breaks=100,
     xlab="Age",
     main="Histogram w/ 100 breaks"
     )


# Sorted Index Plot: Allows you to see all cases individually. Easy to identifiy outliers and not misleading like histograms might be due to selection of bins.
plot(
  sort(na.omit(train$Age)), 
  pch=".",
  ylab = "Age",
  xlab = "Sorted Row Number",
  main="Sorted Index Plot"
  )
```
######################################################################################################################################################################################
######################################################################################################################################################################################
##############################################################     VISUALIZING CATEGORICAL VS. CONTINUOUS DATA     ###################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################

```{r}
# Strip Charts: View categorical vs. continuous
stripchart(
  train$Age~train$Embarked,
  main="Multiple stripchart for comparision",
  xlab="Age",
  ylab="Embarked Status",
  method="jitter",
  col=c("blue","pink","purple"),
  pch=10
  )

stripchart(
  train$Fare~train$Embarked,
  main="Multiple stripchart for comparision",
  xlab="Fare",
  ylab="Embarked Status",
  method="jitter",
  col=c("blue","pink","purple"),
  pch=10
  )
```
######################################################################################################################################################################################
######################################################################################################################################################################################
#############################################################        VISUALIZING BIVARIATE CATEGORICAL DATA       ####################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################

```{r}
# Categorical vs Categorical

# Make a contingency table which shows frequencies in each category
twoWay <- table(train$Embarked, train$Sex)

# Use contingency table to plot Dot Chart
dotchart(twoWay)

# Or make a mosaic plot
mosaicplot(
  twoWay,
  color=TRUE,
  main=NULL,
  las=1
)
```
######################################################################################################################################################################################
######################################################################################################################################################################################
#############################################################        PARAMETRIC VS. NONPARAMETRIC TESTS           ####################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################

Parametric Tests:
An assumption of normality is required for parametric tests. The assumption of normality ensures that sample distribution of a data set is normally distributed. A normal distribution curve is a symmetrical bell-shaped with a mean of zero (0) and a standard deviation of one. If a distribution is not normally distributed, then it is either skewed to the right or skewed to the left. The central limit theorem is used to address a skewed data distribution. The central limit theorem assumed that when we select a large sample from a population, the distribution of the data tends to be normal. The central limit theorem also assumed that a sample of 30 and more are considered large enough sample to make a distribution normal.

```{r}
# Plot histogram
avg <- mean(train$Age)
std <- sqrt(var(train$Age))
hist(train$Age, prob=TRUE)
curve(dnorm(x, mean=avg, sd=std),add=TRUE)

#Shapiro-Wilk test for normality
shapiro.test(train$Age)
```
The null hypothesis is that the distribution of Age is normal. Since the p-value is less than a value of .05, we reject the null hypothesis (thus values for Age are not normal).

```{r}
# Plot histogram
avg <- mean(train$Fare)
std <- sqrt(var(train$Fare))
hist(train$Fare, prob=TRUE)
curve(dnorm(x, mean=avg, sd=std),add=TRUE)

#Shapiro-Wilk test for normality
shapiro.test(train$Fare)
```
The null hypothesis is that the distribution of Fare is normal. Since the p-value is less than a value of .05, we reject the null hypothesis (values for Fare are not normal).

######################################################################################################################################################################################
######################################################################################################################################################################################
########################################################################       PEARSON CORRELATION TEST  (PARAMETRIC) ############################################################################
######################################################################################################################################################################################
######################################################################################################################################################################################
1) Test Assumptions for statistical test (i.e. normality, number of obs. etc.)

2) Make your own assumption & Visualization: 
  Age increases with fare price

```{r}
# Pearson Correlation Coefficient to test linear relationship between two continuous variables (Parametric test so both variables must be normally distributed!!!)
plot(
  Fare~Age, 
  train
  )
# Add a slope line which intersects between most points
abline(0,1)

plot(
  Age~Fare, 
  train
  )
# Add a slope line which intersects between most points
abline(0,1)
```

Statistical Test & Hypothesis:

  While a scatterplot models the association between two continuous variables, the Pearson correlation test mathematically assesses the connection between them . The null and alternative hypothesis for the correlation test, examined in Table 1, are listed as follows: 

  H0: ρ = 0 (There is no linear correlation between variables AGE and FARE)
  H1: ρ ≠ 0 (There is a linear correlation between variables AGE and FARE)

  Where ρ represents the Correlation coefficient statistic.A score close to 0 typically means the two variables are not linearly correlated while a score close to 1 or -1 means they have a strong linear relationship. This relationship can be negative (as one variable increases the other decreases) or positive (as one variable increases the other increases).

  
```{r}
# Run Pearson's correlation test
cor.test(train$Age, train$Fare)
```
Explain Results:
The table above p-value of 0.0102 which is less than the chosen significance level of 0.05, so there is significant evidence to reject the null hypothesis. The results of the correlation test indicate there is a linear correlation between variable AGE and FARE. 

Above table shows the correlation coefficient is equal to 0.09606669, which means there is a small correlation between AGE AND FARE.  In conclusion, the assumption that as the passengers' AGE increases, the cost of their FARE increases was reinforced by the statistical analysis performed above.


######################################################################################################################################################################################
######################################################################################################################################################################################
####################################################################   CHI-SQUARE TEST FOR INDEPENDENCE OR EQUAL PROPORTIONS  ########################################################
######################################################################################################################################################################################
######################################################################################################################################################################################

Assumption & Visualization: 
  Your survival category depends on your gender
  
```{r}
# Categorical vs Categorical

# Make a contingency table which shows frequencies in each category
twoWay_ <- table(train$Survived, train$Sex)
twoWay_

# Use contingency table to plot Dot Chart
dotchart(twoWay_)

# Or make a mosaic plot
mosaicplot(
  twoWay_,
  color=TRUE,
  main=NULL,
  las=1
)
```
Statistical Test & Hypothesis:

The above table, shows that 577 males and 314 women were passengers on the Titanic. Of the 577 males, 109 (18.9%) survived while 468 (81.1%) did not. Of the 314 females, 233 (14.1%) survived while 81 did not. After looking at the summary data, it seems that women are more likely to survive. Therefore, there appears to be some association between the
passenger’s sex and survival on the Titanic. A Chi-Square test analyzes the association between two categorical from the same population and can be used to confirm or deny this inference. The null and alternative hypothesis for the Chi-Square test is as follows:
  
  H0: Passenger’s gender (SEX) and their survival are independent of one another
  H1: Respondent’s gender (SEX) and their survival are dependent of one another
  
```{r}
# Chi Square test for equal proportions
prop.test(twoWay_)
```
The table above displays a p-value (Asymptotic Significance) of 0.00 which is less than the chosen significance level of 0.05, so there is significant evidence to reject the null hypothesis. The results of the Chi-Square test indicate there is an association between the passenger’s sex and survival on the Titanic. In conclusion, there is an association between the passenger’s gender and whether they would have survived on the Titanic which was supported by the statistical analysis performed above.