---
title: "Exploratory Data Analysis in R 103"
output:
  pdf_document: default
  html_notebook: default
---
```{r}
# Import data from CSV file
test <- read.csv("C:/Users/Tells/Downloads/test.csv")

# View first 5 rows
head(test)
```

```{r}
# Obtain basic summary statistics
summary(test)
hist(test$Age)
hist(train$Age)
```


#####################################################################################################################################################################################
#####################################################################################################################################################################################
#############################################################################           T-Test         ##############################################################################
#####################################################################################################################################################################################
#####################################################################################################################################################################################


Two Sample T-test for Equal Means
  + The independent t-test requires that the dependent variable is approximately normally distributed within each group.
  + Assumes the variances of the two groups you are measuring are equal in the  (F-test for equal variances; Levene's; )
  
  Null Hypothesis is that the means are equally distributed (H0: μ1−μ2=0)
  
```{r}
# First step, train for normality
#Shapiro-Wilk train for normality
shapiro.test(train$Age)
```

The null hypothesis is that the distribution of Age is normal. Since the p-value is less than a value of .05, we reject the null hypothesis (thus values for Age are not normal)

+ If not normally distributed we can:
  + Transform the data
  + Bootstrap: Sample the data with replacement to see if it normalizes the data
  + Run the Mann-Whitney U Test for Equal Score Distributions
  
```{r}
# Let's try to transform the data
hist(train$Age)

# Our data is right skewed so let's try square root, cubed root, and log to see if these help

# Square Root
hist(sqrt(train$Age))

# Cube Root
#(cuberoot(train$Age))

# Log
hist(log(train$Age))

# Run new normality tests
shapiro.test(sqrt(train$Age))
shapiro.test(log(train$Age))
```


```{r}
# Let's try to bootstrapping the data
bootstrapTrainAge <- sample(train$Age, 
                         5000, 
                         replace = TRUE
                         )


# Our data is right skewed so let's try square root, cubed root, and log to see if these help

# Square Root
hist(bootstrapTrainAge)

# Run new normality test
shapiro.test(bootstrapTrainAge)
```

```{r}
# Pooled T-test (Parametric 2 Independent Samples - survived vs. not survived; male vs. female)

# First test for equal variances
var.test(Age ~ Survived, train, 
         alternative = "two.sided")

# We know that the data is not normally distributed, but let's look at the differences between the test outcomes
t.test(Age ~ Survived, train, 
         alternative = "two.sided")

# Mann-Whitney U Test (Non-parametric Test)
# Does not test equal means, but rather shift in the location of the whole distribution. Null hypothesis is that there is not shift in the distribution.
wilcox.test(Age ~ Survived, train, 
         alternative = "two.sided")

```
```{r}
# Paired T-test (Parametric 2 Dependent Samples - training vs. testing; before vs. after)
# Must meet the normality and equal variance assumptions

# Let's run the test on training vs. testing Age
t.test(train$Age,
       sample(test$Age, 891, TRUE),
       paired=TRUE
       )

# Mann-Whitey U Test (Non-parametric Test)
wilcox.test(train$Age,
       sample(test$Age, 891, TRUE),
       paired=TRUE)

```

#####################################################################################################################################################################################
#####################################################################################################################################################################################
#############################################################################           Linear Regression         ##############################################################################
#####################################################################################################################################################################################
#####################################################################################################################################################################################

Linear/Logistic Regression Assumptions
  + Errors assumed to be independent
  + Errors assume to have equal variance
  + Errors are normally distributed
  + Assume data can be modeled with a linear pattern
  + Assume no unusual observations exist
ALL THESE ASSUMPTIONS MUST BE TESTED  

```{r}
# Binomial Logistic Regression
# Predicting a value of 0 and 1 for survived which fits the Binomial Distribution
mdl <- glm(Survived ~ Age + Sex + Fare + Embarked,
           family = binomial,
           na.omit(train))

summary(mdl)
```
```{r}
# Use test set to predict probabilities
p <- predict(mdl,
        test,
        type = "response")

# Print test data to screen
head(test, 10)
# Print predicted probabilities to screen
head(p, 10)

plot(p~test$Age)
```


#####################################################################################################################################################################################
#####################################################################################################################################################################################
#############################################################################       Decision Trees         ##############################################################################
#####################################################################################################################################################################################
#####################################################################################################################################################################################

Decision trees, or prediction trees, use a tree structure to specify sequences of decisions and consequences.
  (i.e. If Age is greater than 50 AND Sex is Male
        THEN Survived = 1)
- Can build tree splits based on:
  - Entropy: Impurity of the attribute
  - Information gain: Purity of the attribute
```{r}
#install.packages("rpart.plot")
library("rpart")
library("rpart.plot")

# Build a tree with minimum number of splits as 1 and splits on Information Gain
fit <- rpart(Survived ~ Age + Sex + Fare + Embarked,
             method = "class",
             control = rpart.control(minsplit=1),
             parms=list(split="information"),
             data = train)

summary(fit)
```

```{r}
# Plot the tree splits
rpart.plot(fit, 
           type=4, 
           extra=1)
```

```{r}
# Predict probabilities
head(predict(fit,
       newdata=test,
       type="prob"))
```

```{r}
# Predict classifications
head(predict(fit,
       newdata=test,
       type="class"))
```

# Confusion Matrices or Metrics
False Positives, True Positives, False Negatives, True Negatives