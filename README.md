# EDA in R for Beginners

A series of helpful R notebooks with notes which beginners can use to start learning and practicing Exploratory Data Analysis in R. The dataset comes from Kaggle.com Titanic competition.